package decathlon;

import java.util.Map;

public class RankEngine {
	
	private static boolean measureManual = false; //-because it is undefined in the input set if it is manual measure or not
	
	private void checkForManual(Athlete athlete) throws Exception{
		if (measureManual) {
        //not supported
			throw new Exception("Unsuported feature");
		}
	}
	
	public int calcPoints(Events key, Float score) {
	
		if (key.name().endsWith("M")) {
			return (int) (key.getA() * Math.pow(key.getB() - score, key.getC()));
		} else {
			return (int) (key.getA() * Math.pow(score - key.getB(), key.getC()));
		}
		
	}
	
	public RankEngine() {
	
	}
		
	public int calcTotalPoints(Athlete athlete) throws Exception{
		
		int totalPoints = 0;
		for (Map.Entry<Events, Float> score : athlete.getScores().entrySet()) {
			checkForManual(athlete);
			int points = calcPoints(score.getKey(), score.getValue());
			totalPoints += points;
		}
		return totalPoints;
	}
}
