package decathlon;

import java.util.Map;

public class Athlete implements Comparable<Athlete>{
	
  private String name;
  private Map<Events, Float> scores;
  private int points;
  private String place;
  
  public Athlete(){}
  
  public String getName() {
	  return name;
  }
  public void setName(String name) {
	  this.name = name;
  }
    
  public Map<Events, Float> getScores() {
	  return scores;
  }
  
  public void setScores(Map<Events, Float> scores) {
	  	this.scores = scores;
  }

public int getPoints() {
	return points;
}

public void setPoints(int points) {
	this.points = points;
}

public String getPlace() {
	return place;
}

public void setPlace(String place) {
	this.place = place;
}

public int compareTo(Athlete athlete) {
	return -new Integer(getPoints()).compareTo(athlete.getPoints());
}
  
}
