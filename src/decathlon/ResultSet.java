package decathlon;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  

@XmlRootElement  
public class ResultSet {

	private List<Result> athletes;
	
	public ResultSet(){
		
	}

	@XmlElement  
	public List<Result> getResultSet() {
		return athletes;
	}

	public void setResultSet(List<Result> athletes) {
		this.athletes = athletes;
	}	
}
