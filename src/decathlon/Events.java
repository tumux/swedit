package decathlon;

public enum Events {
	EV_100M { 
		
		public float getA(){
			return 25.4347f;
		}
		
		
		public float getB(){
			return 18f;
		}
		
		
		public float getC(){
			return 1.81f;
		}		
	},
	
	LONG_JUMP{ 
		public float getA(){
			return 0.14354f;
		}
		
		
		public float getB(){
			return 220f;
		}
		
		
		public float getC(){
			return 1.4f;
		}	
	},
	
	SHOT_PUT{ 
		public float getA(){
			return 51.39f;
		}
		
		
		public float getB(){
			return 1.5f;
		}
		
		
		public float getC(){
			return 1.05f;
		}	
	},
	HIGH_JUMP{ 
		public float getA(){
			return 0.8465f;
		}
		
		
		public float getB(){
			return 75f;
		}
		
		
		public float getC(){
			return 1.42f;
		}	
	},
	EV_400M{ 
		public float getA(){
			return 1.53775f;
		}
		
		
		public float getB(){
			return 82f;
		}
		
		
		public float getC(){
			return 1.81f;
		}	
	},
	EV_110M{ 
		public float getA(){
			return 5.74352f;
		}
		
		
		public float getB(){
			return 28.5f;
		}
		
		
		public float getC(){
			return 1.92f;
		}	
	},
	DISCUS_THROW{ 
		public float getA(){
			return 12.91f;
		}
		
		
		public float getB(){
			return 4f;
		}
		
		
		public float getC(){
			return 1.1f;
		}	
	},
	POLE_VAULT{
		public float getA(){
			return 0.2797f;
		}
		
		
		public float getB(){
			return 100f;
		}
		
		
		public float getC(){
			return 1.35f;
		}	
	},
	JAVELIN_THROW{ 
		public float getA(){
			return 10.14f;
		}
		
		
		public float getB(){
			return 7f;
		}
		
		
		public float getC(){
			return 1.08f;
		}	
	},
	EV_1500M{ 
		public float getA(){
			return 0.03768f;
		}
		
		
		public float getB(){
			return 480f;
		}
		
		
		public float getC(){
			return 1.85f;
		}	
	};
	
	public abstract float getA();
	
	public abstract float getB();
	
	public abstract float getC();
}

