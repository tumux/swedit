package test;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

import decathlon.Events;
import decathlon.RankEngine;

public class RankEngineTest {

	@Test
	public void calcPoints(){
		
		RankEngine ranker = new RankEngine();
		assertEquals(536, ranker.calcPoints(Events.EV_100M, 12.61f));
		assertEquals(382, ranker.calcPoints(Events.LONG_JUMP, 500f));
		assertEquals(439, ranker.calcPoints(Events.SHOT_PUT, 9.22f));
		assertEquals(389, ranker.calcPoints(Events.HIGH_JUMP, 150f));
		assertEquals(400, ranker.calcPoints(Events.EV_400M, 60.39f));
		assertEquals(685, ranker.calcPoints(Events.EV_110M, 16.43f));
		assertEquals(302, ranker.calcPoints(Events.DISCUS_THROW, 21.6f));
		assertEquals(264, ranker.calcPoints(Events.POLE_VAULT, 260f));
		assertEquals(382, ranker.calcPoints(Events.JAVELIN_THROW, 35.81f));
		assertEquals(421, ranker.calcPoints(Events.EV_1500M, 325.72f));
		
	}
}
