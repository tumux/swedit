package main;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import decathlon.Athlete;
import decathlon.Events;
import decathlon.Result;
import decathlon.ResultSet;

public class FileReader {
  private static final String path = "ext/"; 
  private static final String recordDelimiter = ";";
  private String inputFileName;
  
  private List<Athlete> parseLines(List<String> lines) {
	  
	  List<Athlete> athletes = new ArrayList<Athlete>();
	  for (String line : lines) {		  
		  Athlete athlete = new Athlete();
		  Map<Events, Float> scores = new HashMap<Events, Float>();
		  int i = 0;
		  for (String subItem : line.split(recordDelimiter)) {
			  switch (i) {
			  case 0: athlete.setName(subItem);
      		  break;
			  case 1: scores.put(Events.EV_100M, Float.valueOf(subItem));
			  break;
			  case 2: scores.put(Events.LONG_JUMP, Float.valueOf(subItem)*100);
	          break;			  
			  case 3: scores.put(Events.SHOT_PUT, Float.valueOf(subItem));
      		  break;
			  case 4: scores.put(Events.HIGH_JUMP, Float.valueOf(subItem)*100);
			  break;
			  case 5: scores.put(Events.EV_400M, Float.valueOf(subItem));
      		  break;
			  case 6: scores.put(Events.EV_110M, Float.valueOf(subItem));
      		  break;
			  case 7: scores.put(Events.DISCUS_THROW, Float.valueOf(subItem));
      		  break;
			  case 8: scores.put(Events.POLE_VAULT, Float.valueOf(subItem)*100);
      		  break;
			  case 9: scores.put(Events.JAVELIN_THROW, Float.valueOf(subItem));
      		  break;
			  case 10: String[] runTime = subItem.split("\\."); 
			  Float runTimeParsed = Float.valueOf(runTime[0])*60 + Float.valueOf(runTime[1]) + Float.valueOf(runTime[2])/100;
			           scores.put(Events.EV_1500M, runTimeParsed);
			           break;
			  }			 
			i++;  
		  }		
		  athlete.setScores(scores);
		  athletes.add(athlete);
	  }
	  
	  return athletes;
  }
  private List<String> readLines(){
	  List<String> inputLines = new ArrayList<String>();
	  String fileName = path.concat(inputFileName);
	  try {
		  for (String line : Files.readAllLines(Paths.get(fileName))) {
			  inputLines.add(line); 
		  }
	  }
	  catch (IOException e) {
		  return null;
	  }
	  
	  return inputLines;
  }

  public FileReader(String inputFileName){
	  this.inputFileName = inputFileName;
  }
  
  public List<Athlete> parseInput() {
	  List<String> lines = readLines();
	  List<Athlete> athletes = parseLines(lines);
	  return athletes;
  }
  
  public void generateOutput(List<Athlete> athletes, String outputFileName) throws JAXBException, FileNotFoundException{
	  
	  ResultSet resultSet = new ResultSet();
	  List<Result> resultList = new ArrayList<Result>();
	  for (Athlete athlete : athletes) {
		Result result = new Result();
		result.setName(athlete.getName());
		result.setRank(athlete.getPlace());
		result.setScore(athlete.getPoints());	
		resultList.add(result);
	  }
	  resultSet.setResultSet(resultList);
      JAXBContext jc = JAXBContext.newInstance(ResultSet.class);
      Marshaller marshaller = jc.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      marshaller.marshal(resultSet, System.out);
      String fileName = path.concat(outputFileName);
      OutputStream os = new FileOutputStream(fileName);
      marshaller.marshal( resultSet, os );
  }
}
