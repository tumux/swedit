package main;

public class InputValidator {
	String[] args;
	public InputValidator(String[] args) {
		this.args = args;
	}
	public void validateInput() throws Exception{
		if (args.length != 2) {
			throw new Exception("Wrong number of input parameters. Will accept two only. Now there is " + args.length);
		}
		
		if (!args[0].endsWith(".csv")) {
			throw new Exception("Wrong input file format. For now only csv.");
		}
		
		if (!args[1].endsWith(".xml")) {
			throw new Exception("Wrong input file format. For now only csv.");
		}
	}
	
}
