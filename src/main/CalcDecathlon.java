package main;

import java.util.Collections;
import java.util.List;

import decathlon.Athlete;
import decathlon.RankEngine;

public class CalcDecathlon {

	public static void main(String[] args) throws Exception {
		
		//1. validate Input
		InputValidator inputValidator = new InputValidator(args);
		inputValidator.validateInput();
		//2.Read Input file to athlete Object 
		FileReader fileReader = new FileReader(args[0]);
		List<Athlete> athletes = fileReader.parseInput(); 				
		//3 Calculate scores for athletes
		RankEngine ranker = new RankEngine();
		for (Athlete athlete : athletes){
			athlete.setPoints(ranker.calcTotalPoints(athlete));
		}
		
		Collections.sort(athletes);
		int i = 1;
	    for (Athlete athlete : athletes) {
	    	athlete.setPlace(String.valueOf(i));
	    	i++;
	    }
		//4 parseToResultSet sort it too
		fileReader.generateOutput(athletes, args[1]);
		//5 output XML
		
	}

}
